import React, { Component } from 'react';
import classes from './Search.module.css';

class Search extends Component {


    render() {
        return (
            <div className={classes.Search}>
                <form class="" method="get" action="/search">
                    <div className={classes['controls-wrapper']}>
                        <div className={classes['search-controls']}>
                            <input type="text" id="searchform-q" name="q" placeholder="Search Shows and People" />
                            <div className={''}>
                                <button type="submit" class="medium button postfix" alt="Search"></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default Search;