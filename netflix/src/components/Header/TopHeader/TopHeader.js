import React from 'react';
import classes from './TopHeader.module.css';
import logo from '../../../img/header-logo.png';
import Search from './Search/Search';
import UserAdmin from './UserAdmin/UserAdmin'


const TopHeader = props => {


    return (
        <React.Fragment>
            <div className={classes['top-header']}>
                <a href="#">
                    <img src={logo} alt={'logo'} />
                </a>
            <Search />
            <UserAdmin />
            </div>
        </React.Fragment>
    )
}

export default TopHeader;