import React from 'react';
import './App.module.css';
import Layout from './hoc/layout/Layout'

function App() {
  return (
    <React.Fragment>
      <Layout />
    </React.Fragment>
  )
}

export default App;
